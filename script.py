import sys
from pymongo import MongoClient
import pprint
from collections import deque

def connect():
    client = MongoClient('mongodb://root:qXwrJG4SgJL5@35.200.243.65:27017')
    first_db = 'solo-hydra-test-management-dev-10'
    second_db = 'luke-hydra-test-management-dev-beta'
    return client[first_db], client[second_db]

def cwuAnalysis(solo, luke, student_id):
    error = []
    lst = ['Maths', 'Physics', 'Chemistry', 'overall']
    for entry in lst:
        luke_tmp = luke[entry]
        del luke_tmp['marks']
        solo_tmp = [t['data'] for t in solo if t['subject'] == entry]
        solo_tmp = solo_tmp[0]
        if luke_tmp != solo_tmp:
            error.append('CWU Mismatch for {} for student {}'.format(entry, student_id))
    
    return error

def hierarchyLevels(solo, luke, student_id):
    return solo != luke

def studentMetaData(solo, luke, student_id):
    return solo['hierarchy'] != luke['hierarchy']

def responseData(solo, luke, student_id):
    return solo['questionResponse'] != luke['questionResponse']

def rankAnalysis(solo, luke, student_id):
    if luke:
        error = []
        lst = ['Maths', 'Physics', 'Chemistry', 'overall']
        solo_rank = {}
        for item in solo:
            solo_rank[item['subject']] = item['data']['rank']
        for item in lst:
            if luke['rankAnalysis'][item]['rank'] != solo_rank[item]:
                error.append('Mismatch rank for subject {} for {}'.format(item, student_id))
        return error
    return []

def run(test_id):
    first_db, second_db = connect()
    solo_master_result = first_db['masterresults']
    luke_master_result = second_db['masterresults']
    luke_rank_analysis = second_db['rankAnalysis']
    solo_test_list = solo_master_result.find({'testId': test_id})

    cnt = 0
    masterresults_error = []
    for doc in solo_test_list:
        luke_doc = luke_master_result.find_one({'testId': test_id, 'studentId': doc['studentId']})
        luke_doc_rank = luke_rank_analysis.find_one({'testId': test_id, 'student': doc['studentId']})

        not_ok_cwu = cwuAnalysis(doc['cwuAnalysis'], luke_doc['cwuAnalysis'], doc['studentId'])
        if not_ok_cwu:
            masterresults_error.append ('CWU mismatch for {}'.format(doc['studentId']))

        not_ok_hierarchy_levels = hierarchyLevels(doc['hierarchyLevels'], luke_doc['hierarchyLevels'], doc['studentId'])
        if not_ok_hierarchy_levels:
            masterresults_error.append ('Hierarchy Levels mismatch for {}'.format(doc['studentId']))

        not_ok_student_meta_data = studentMetaData(doc['studentMetaData'], luke_doc['studentMetaData'], doc['studentId'])
        if not_ok_student_meta_data:
            masterresults_error.append ('Student meta data mismatch for {}'.format(doc['studentId']))

        not_ok_question_response = responseData(doc['responseData'], luke_doc['responseData'], doc['studentId'])
        if not_ok_question_response:
            masterresults_error.append ('Response data mismatch for {}'.format(doc['studentId']))

        not_ok_rank_analysis = rankAnalysis(doc['rankAnalysis'], luke_doc_rank, doc['studentId'])
        if not_ok_rank_analysis:
            masterresults_error.append ('Rank analysis mismatch for {}'.format(doc['studentId']))

        cnt += 1
        if (cnt%100 == 0):
            print ("Completed processing {} records for masterresults".format(cnt))
        # if cnt >= 500:
        #     break

    print ("Master results errors", masterresults_error)

    cnt = 0
    # alltestsdatastore collection
    alltestsdatastore_error = []
    luke_all_test = second_db['alltestsdatastore'].find({'testId': test_id})
    for item in luke_all_test:
        student_id = item['studentId']
        solo_all_test = first_db['alltestsdatastore'].find({'studentId': student_id, 'testId': test_id})
        for doc in solo_all_test:
            not_ok_alltestsdatastore = allTestsDataStore(doc, item, student_id, test_id)
            if not_ok_alltestsdatastore:
                alltestsdatastore_error.append('Mismatch for {} level for student {}'.format(doc['level'], student_id))
        
        cnt += 1
        if cnt%100 == 0:
            print ('Completed processing {} docs for alltestsdatastore'.format(cnt))
        # if cnt >= 500:
        #     break
    print ('alltestsdatastore error', alltestsdatastore_error)


    hierarchical_analysis_errors = []
    cnt = 0
    solo_ha_all = first_db['hierarchicalAnalysis'].find({'testId': test_id})
    for item in solo_ha_all:
        _id = item['childCode'] + '_' + test_id
        luke_doc = second_db['hierarchicalAnalysis'].find({'_id': _id})
        for t in luke_doc:
            not_ok_hierarchy_analysis = hierarchicalAnalysis(t, item)
            if not_ok_hierarchy_analysis:
                hierarchical_analysis_errors.append('Mismathc for {} child code'.format(item['childCode']))
        cnt += 1
        if cnt%100 == 0:
            print ('Processed {} records for hierarchical analysis'.format(cnt))
        # if cnt >= 500:
        #     break
    print ('Hierarchical analysis errors', hierarchical_analysis_errors)

def hierarchicalAnalysis(luke, solo):

    luke_qa = luke['questionMap']
    solo_qa = solo['questionAnalysis']
    for question in solo_qa:
        del solo_qa[question]['P']

    qa_ok = luke_qa == solo_qa

    # pprint.pprint(luke_qa)
    # print('-------------')
    # pprint.pprint(solo_qa)

    luke_ta = luke['topicAnalysis']
    solo_ta = solo['topicAnalysis']
    d_one, d_two = [luke_ta], [solo_ta]

    ok = True

    while d_one:
        item = d_one.pop(0)
        tmp = d_two.pop(0)

        same_fields = ['ADD', 'C', 'P', 'U', 'W', 'totalMarks', 'totalObtainedMarks', 'totalQuestion']

        for level in item:
            item_down_level = item[level]
            tmp_down_level = tmp[level]
            for field in same_fields:
                ok &= item_down_level[field] == tmp_down_level[field]
            # if not ok:

            #     pprint.pprint(item_down_level)
            #     print ('------------')
            #     pprint.pprint(tmp_down_level)
            #     print ('____________')
            #     return
            if item_down_level['next']:
                d_one.append(item_down_level['next'])
                d_two.append(tmp_down_level['next'])
    return not (ok and qa_ok)

def allTestsDataStore(solo_doc, luke_doc, student_id, test_id):
    if solo_doc['level'] == 'subject':
        luke_doc_sub = luke_doc['subjectLevelResults']
        lst = ['Chemistry', 'Maths', 'Physics']
        for item in lst:
            del luke_doc_sub[item]['errorPercentage']
            del luke_doc_sub[item]['totalQuestions']
        ok = True
        for item in lst:
            ok &= solo_doc[test_id][item] != luke_doc_sub[item]
        return not ok
    elif solo_doc['level'] == 'topic':
        luke_doc_topic = luke_doc['topicLevelResults']
        for item in luke_doc_topic:
            del luke_doc_topic[item]['errorPercentage']
            del luke_doc_topic[item]['parent']
            del luke_doc_topic[item]['percentage']
        return luke_doc_topic != solo_doc[test_id]
    elif solo_doc['level'] == 'subtopic':
        luke_doc_sub_topic = luke_doc['topicLevelResults']
        for item in luke_doc_sub_topic:
            del luke_doc_sub_topic[item]['errorPercentage']
            del luke_doc_sub_topic[item]['parent']
            del luke_doc_sub_topic[item]['percentage']
            del luke_doc_sub_topic[item]['subject']
        return luke_doc_sub_topic != solo_doc[test_id]
    return False


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please provide test ID')
    else:
        test_id = sys.argv[1]
        run(test_id)
